LowKeyThicc: Version 1
Author: Nick Caradonna

Automatically deletes history of any porn/possible NSFW page immediately upon visiting the page. I have tried a couple extensions for this purpose and nothing has worked, so I wrote my own. Enjoy.

Note: Does not delete past history.
Note: I am not liable for anything.

You can contact me with specific keywords you want to automatically delete and I will customize the extension for you.